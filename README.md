Создание декоратора класса


Представьте себя ненадолго разработчиком компьютерной игры в стиле фэнтези. Вы будете прописывать систему эффектов, которые могут быть наложены на героя вашей игры.

У вас есть герой, который обладает некоторым набором характеристик. Враги и союзники могут накладывать на героя положительные и отрицательные эффекты. Эти эффекты каким-то образом изменяют характеристики героя. На героя можно накладывать бесконечно много эффектов, действие одинаковых эффектов суммируется. Игрок должен знать, какие положительные и какие отрицательные эффекты на него были наложены и в каком порядке.

Класс герой описан следующим образом (характеристики могут быть другими):

class Hero:
    def __init__(self):
        self.positive_effects = []
        self.negative_effects = []
        
        self.stats = {
            "HP": 128,
            "MP": 42,
            "SP": 100,
            
            "Strength": 15,
            "Perception": 4,
            "Endurance": 8,
            "Charisma": 2,
            "Intelligence": 3,
            "Agility": 8,
            "Luck": 1
        } 
        
    def get_positive_effects(self):
        return self.positive_effects.copy()
    
    def get_negative_effects(self):
        return self.negative_effects.copy()
    
    def get_stats(self):
        return self.stats.copy()
        
    ...

Описывать класс героя в коде НЕ НУЖНО.

Вам нужно написать систему декораторов, представленную на UML-диаграмме: <see uml.jpg in the repo>

 


Названия наложенных положительных и отрицательных эффектов добавляются каждое в свой счетчик. Названия эффектов совпадают с названиями классов.

Описания эффектов:

1. Берсерк — Увеличивает параметры Сила, Выносливость, Ловкость, Удача на 7; уменьшает параметры Восприятие, Харизма, Интеллект на 3. Количество единиц здоровья увеличивается на 50.
2. Благословение — Увеличивает все основные характеристики на 2.
3. Слабость — Уменьшает параметры Сила, Выносливость, Ловкость на 4.
4. Сглаз — Уменьшает параметр Удача на 10.
5. Проклятье — Уменьшает все основные характеристики на 2.

К основным характеристикам относятся Сила (Strength), Восприятие (Perception), Выносливость (Endurance), Харизма (Charisma), Интеллект (Intelligence), Ловкость (Agility), Удача (Luck).

При выполнении задания учитывайте, что:

1. Изначальные характеристики базового объекта не должны меняться.
2. Изменения характеристик и накладываемых эффектов (баффов/дебаффов) должно происходить динамически, то есть при запросе get_stats, get_positive_effects, get_negative_effects
3. Абстрактные классы AbstractPositive, AbstractNegative и соответственно их потомки могут принимать любой параметр base при инициализации объекта (_ _ init _ _ (self, base))
4. Проверяйте, что эффекты корректно снимаются, в том числе и из середины стека
5. Для тестирования правильности работы вашего решения вы можете повторить пример работы приведенный ниже или использовать тестовый скрипт. Заметим, что данные примеры не покрывают всех тестовых случаев, проверяемых тестовой системой, это всего лишь отправная точка для ваших тестов.